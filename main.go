package main

import (
 "fmt"
 "bytes"
 "log"
 "net/http"
 "time"
 "os"
)

func indexHandler(w http.ResponseWriter, req *http.Request) {
	datetime := time.Now()
	time := datetime.Format("15:04")
	fmt.Printf("%s", time)
}

func addHandler(w http.ResponseWriter, req *http.Request) {
    if err := req.ParseForm(); err != nil {
	    fmt.Println("Something went bad")
	    fmt.Fprintln(w, "Something went bad")
	    return
	}	

	req.ParseForm()
	author := req.Form.Get("author")
	entry := req.Form.Get("entry")

	fmt.Println(author)

	file, err := os.OpenFile("d.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
        panic(err)
    }

	defer file.Close()
	_, err2 := file.WriteString(author + ":" + entry + "\n")

	if err2 != nil {
        panic(err)
	}

	fmt.Fprintf(w, author+":"+entry)
}


func entriesHandler(res http.ResponseWriter, req *http.Request) {

	filer, err := os.Open("d.txt")
	if err != nil {
		log.Fatal(err)
	}

	defer filer.Close()

	buf := new(bytes.Buffer)
	buf.ReadFrom(filer)

	fmt.Fprintf(res, "%v", buf.String())
}


func main() {
	http.HandleFunc("/", indexHandler)
	http.HandleFunc("/add", addHandler)
	http.HandleFunc("/entries", entriesHandler)
	http.ListenAndServe(":4567", nil)
}